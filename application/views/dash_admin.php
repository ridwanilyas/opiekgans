<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css"  media="screen,projection"/>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class="blue accent-3">
	<div class="container" style="width: 40%;">
		<ul id="slide-out" class="side-nav fixed">
        <li><a href="<?php echo base_url(); ?>kelolaUser" style="letter-spacing:1.5px;"><i class="material-icons">supervisor_account</i>User Management</a></li>
        <li><a href="<?php echo base_url(); ?>welcome" onclick="return confirm('Apakah anda yakin ingin log out?')" style="letter-spacing:1.5px;"><i class="material-icons">power_settings_new</i>Log Out</a></li
        <li style="margin-top:30px;">
            <div class="divider"></div>
        </li>
        <li><a class="subheader">&copy; 2019 Fikri,Gerry,Ridwan</a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
    <div class="row" style="margin-left:280px;">
        <div class="container" style="width:90%;">
            <h1 class="white-text align-center" style="font-size: 50px;margin-top: 0px;line-height: 110px">Welcome,<br>
                <span style="font-weight:bold;font-size: 80px;">Fikri,Gerry,Ridwan</span></h1>
            <div class="divider" style="height:3px;"></div>
        </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/materialize.min.js"></script>
    <script type="text/javascript"></script>

</body>
</html>