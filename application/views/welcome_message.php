<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
  
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css"  media="screen,projection"/>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class="blue accent-3">
  <div class="container" style="width: 40%;">
    <form method="GET">
      <div class="card" style="margin-top: 15%; border-radius: 10px;padding-top: 2%;padding-bottom: 2%;">
              <h5 class="deep-orange-text text-darken-2 productsans center" style="font-size: 20px; ">Welcome here Adminzzzz</h5>
          <div class="card-content">
                
                <div class="input-field">
                  <i class="material-icons prefix"></i>
                        <input name="emailLogin" id="first_name" type="email" class="validate">
                        <label for="first_name">Email</label>
        </div>

            <div class="input-field">
                  <i class="material-icons prefix"></i>
                        <input name="Password" id="second_name" type="password" class="validate">
                        <label for="second_name">Password</label>
        </div>

        <div class="center">
          <button class="btn waves-effect waves-light " type="submit" name="action" style="width: 30%;border-radius: 100px; ">Login<i class="material-icons right" ></i></button>

        </div>
          </div>
        </div>
    </form>
  </div>
  
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>